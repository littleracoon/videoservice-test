## Используемые технологии

- HTML, CSS, SASS, JS6, react;

- create react app;

- react-app-polyfill, react-custom-scrollbars, react-router-dom

## Как запустить проект

- на компьютере должен быть установлен node.js 

- клонировать или скачать репозиторий

- открыть проект в IDE (например в visual code)

- установить скачать node modules (в терминале написать npm install)

- в терминале написать npm start

- для сборки проекта в терминале написать npm run build
