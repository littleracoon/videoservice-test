import React from 'react';

export default function Body({ children }) {

    return <div className="modal__body">{children}</div>
}