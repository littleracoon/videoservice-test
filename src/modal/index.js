import React from 'react';
import classNames from 'classnames';

import Header from './header';
import Body from './body';
import Footer from './footer';

import './main.scss';

export default function Modal({ show, onHide, children }) {

	const focusedInput = React.createRef();

	return <div className={classNames('modal-hide', { 'in': show })}
		ref={focusedInput}
		onClick={event => event.target === focusedInput.current && onHide()}
	>
		<div className="modal">
			{children}
		</div>
	</div>
};

Modal.Header = Header;
Modal.Body = Body;
Modal.Footer = Footer;
