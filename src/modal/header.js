import React from 'react';

export default function Header({ children }) {

    return <div className="modal__header">{children}</div>
}