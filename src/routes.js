import React from 'react';
import { Redirect } from "react-router-dom";

import Films from './videoservice/pages/films';
import Channels from './videoservice/pages/channels';
import NotFound from './videoservice/pages/not-found';

const Routes = [
  {
    path: '/',
    exact: true,
    render: () => {
      return <Redirect to={'/films'} />;
    },
  },
  {
    id: 'films',
    path: '/films',
    component: Films,
  },
  {
    id: 'channels',
    path: '/channels',
    component: Channels,
  },
  {
    path: '/not-found',
    component: NotFound,
  },
  {
    path: '*',
    component: NotFound,
  },
];

export default Routes;
