import React, { useState } from 'react';
import classNames from 'classnames';
import logo from './layout/sign.svg';
import './App.scss';
import Page from './layout';
import HTCLogo from './layout/htc-logo.png';
import F from './form';
import Search from './videoservice/forms/search';
import Modal from './modal';
import SignInForm from './videoservice/forms/signin';

import Burger from './svg-icons/burger';

import Routes from './routes';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


function App() {
  const [modal, setModal] = useState(false);
  const [isAutorized, setIsAutorized] = useState(Boolean(localStorage.getItem('_vs_user_name')));
  const [userName, setUserName] = useState(localStorage.getItem('_vs_user_name'));
  const [menuOpen, setMenuOpen] = useState(false);

  const changeUserName = (value) => {
    localStorage.setItem('_vs_user_name', value);
    setUserName(value)
  };

  const logIn = (value, callback) => {
    if (value) {
      setIsAutorized(true);
      setUserName(value);
      callback();
    };
    setModal(false);
  };

  const logOut = () => {
    localStorage.setItem('_vs_user_name', '');
    setIsAutorized(false);
    setUserName(null);
  }

  return (
    <Page>
      <Page.Header>
        <div className="container">
          <div className="nav">
            <div className="nav__logo">
              <img src={logo} className="App-logo" alt="logo" />
            </div>
            <button className="nav__toggler" onClick={() => setMenuOpen(!menuOpen)}>
              <Burger color="#333333" />
            </button>
            <div className={classNames('nav__collapse', { 'nav__collapse--active': menuOpen })}>
              <div className="nav__col nav__search">
                <Search className="ml-auto mr-auto ml-lg-3 mr-lg-3" />
              </div>
              <div className="nav__signin">
                {isAutorized ?
                  <>
                    <F.InputButton
                      className="mr-3"
                      value={userName}
                      onChange={(event) => changeUserName(event.target.value)}
                    />
                    <F.Button variant="link" onClick={() => logOut()}>Выйти</F.Button>
                  </> :
                  <F.Button variant="main" onClick={() => setModal(true)}>Войти</F.Button>
                }
              </div>
            </div>

          </div>
        </div>
      </Page.Header>
      <Router>
        <Page.Tabs items={{
          films: {
            name: 'Фильмы'
          },
          channels: {
            name: 'Телеканалы'
          }
        }} />
        <Page.Body>
          <Switch>
            {Routes.map((route, i) => <Route key={i} {...route} />)}
          </Switch>
        </Page.Body>
        <Page.Footer>
          <div className="container">
            <div className="footer-content">
              <div className="footer-content__logo">
                <img src={HTCLogo} alt="htc" />
              </div>
              <div className="footer-content__body">
                <div className="footer-content__text">426057, Россия, Удмуртская Республика, г. Ижевск, ул. Карла Маркса, 246 (ДК «Металлург»)<br />
                +7 (3412) 93-88-61, 43-29-29
                </div>
                <a className="font-weight-bold" href="https://htc-cs.ru/">htc-cs.ru</a>
              </div>
            </div>
          </div>
        </Page.Footer>
      </Router>

      <Modal show={Boolean(modal)} onHide={() => setModal(false)} >
        <Modal.Header>Вход</Modal.Header>

        <Modal.Body>
          <SignInForm id="signinForm" onSubmit={(value, callback) => logIn(value, callback)} />
        </Modal.Body>
        <Modal.Footer>
          <F.Button type="submit" form="signinForm" variant="main">Войти</F.Button>
        </Modal.Footer>

      </Modal>
    </Page >
  )
}

export default App;
