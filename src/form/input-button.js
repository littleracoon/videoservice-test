import React, { useState } from 'react';
import classNames from 'classnames';
import './input-button.scss';

export default function InputButton({ className, ...props }) {

    const [userNameInputType, setUserNameInputType] = useState('button');

    return <input
        type={userNameInputType}
        {...props}
        className={classNames('input-button', className)}
        onFocus={() => setUserNameInputType('text')}
        onBlur={() => setUserNameInputType('button')}
    />
};
