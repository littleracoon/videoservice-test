import React from 'react';
import classNames from 'classnames';
import './input.scss';

export default function Input({ className, ...props }) {

    return <input {...props} className={classNames('form-control', className)} />
};
