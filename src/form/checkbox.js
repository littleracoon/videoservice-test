import React from 'react';
import classNames from 'classnames';
import './checkbox.scss';

export default function Checkbox({ id, className, label, ...props }) {

    return <div className="custom-checkbox">
        <input {...props} type="checkbox" className={classNames('custom-checkbox__input', className)} id={id} />
        <label className="custom-checkbox__label" htmlFor={id}>{label}</label>
    </div>
};
