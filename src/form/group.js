import React from 'react';
import classNames from 'classnames';
import './group.scss';

export default function Group({ className, children }) {

    return <div className={classNames('form-group', className)}>
        {children}
    </div>
};
