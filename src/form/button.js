import React from 'react';
import classNames from 'classnames';

import './button.scss';

export default function Button({ className, children, variant, ...props }) {
    
    return <button className={classNames(
        'button',
        {
            [`button--${variant}`]: variant
        },
        className)} {...props}>{children}</button>
} 