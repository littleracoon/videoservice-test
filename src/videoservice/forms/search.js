import React from 'react';
import classNames from 'classnames';
import F from '../../form';

import './search.scss';

export default function Search({ className, ...props }) {

    return <form className={classNames('search-form', className)} onSubmit={(event) => event.preventDefault()}>
        <F.Input className="search-input" type="text" placeholder="Поиск..." />
        <F.Button type="submit" variant="link">Найти</F.Button>
    </form>
}
