import React, { useState } from 'react';
import F from '../../form';


export default function SignInForm({ id, onSubmit, ...props }) {
    const [values, setValues] = useState({
        login: '',
        password: '',
        rememberMe: false
    });

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(values)
        const { login, rememberMe } = values;

        onSubmit(login, () => rememberMe ? localStorage.setItem('_vs_user_name', login): false);
    };

    const handleChange = (event) => {
        const { target: { name, value, checked, type } } = event;
        const types = {
            checkbox: checked,
            default: value
        };

        setValues({ ...values, [name]: types[type in types ? type : 'default'] })
    };

    return <form id={id} {...props} onSubmit={handleSubmit}>
        <F.Group>
            <F.Input
                name="login"
                type="text"
                value={values.login}
                placeholder="логин"
                onChange={handleChange} />
        </F.Group>
        <F.Group>
            <F.Input name="password" type="password"
                value={values.password} placeholder="пароль" onChange={handleChange} />
        </F.Group>
        <F.Group>
            <F.Checkbox name="rememberMe"
                value={values.rememberMe}
                id="rememberMe"
                label="Запомнить"
                onChange={handleChange}
                value={values.rememberMe}
                checked={values.rememberMe} />
        </F.Group>
    </form>
}