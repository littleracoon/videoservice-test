import React from 'react';

import CardBox from '../../card-box';
import Images from '../../card-box/cards/film-images';

export default function Films() {
	const genres = [
		{
			title: 'Комедии',
			icon: '😁',
			variantColor: 'citrus'

		},
		{
			title: 'Драмы',
			icon: '😭',
			variantColor: 'persic'
		},
		{
			title: 'Фантастика',
			icon: '👽',
			variantColor: 'blue'
		},
		{
			title: 'Ужасы',
			icon: '👻',
			variantColor: 'dark'
		},
	];
	const films = [
		{
			title: 'Мульт в кино. Выпуск №103. Некогда грустить!',
			img: Images.Img1,
			description: ''
		},
		{
			title: 'Новый Бэтмен',
			img: Images.Img2,
			description: ''
		},
		{
			title: 'Однажды... в Голливуде',
			img: Images.Img3,
			description: `Фильм повествует о череде событий, произошедших в Голливуде в 1969 году,
			на закате его «золотого века». Известный актер Рик Далтон и его дублер Клифф Бут пытаются
			найти свое место в стремительно меняющемся мире киноиндустрии.`
		},
		{
			title: 'Стриптизёрши',
			img: Images.Img4,
			description: ''
		},
	]

	return <div className="page-content container">
		<h3><span role="img" aria-label="fire">🔥</span> Новинки</h3>
		<CardBox className="page-content__section">
			{films.map((film, index) => <CardBox.Film
				key={index}
				col="3"
				title={film.title}
				img={film.img}
			>
				{film.description}
			</CardBox.Film>)}
		</CardBox>

		<h3>Жанры</h3>
		<CardBox className="page-content__section">
			{genres.map((genre, index) => <CardBox.Genre
				key={index}
				col="3"
				title={genre.title}
				icon={genre.icon}
				// background={genre.color} 
				variant={genre.variantColor} />
			)}
		</CardBox>

	</div>
};
