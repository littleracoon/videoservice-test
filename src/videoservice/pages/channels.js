import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import CardBox from '../../card-box';
import FirstChannel from '../../card-box/cards/channel-icons/first.svg';

import Channel2x2 from '../../card-box/cards/channel-icons/2x2.svg';
import RBC from '../../card-box/cards/channel-icons/rbc.svg';
import AmediaPremium from '../../card-box/cards/channel-icons/amedia.svg';

export default function Channels() {
	return <div className="page-content container">
		<Scrollbars
			style={{ width: '100%', height: 694 }}
			renderTrackVertical={props => <div {...props} className="track-vertical" />}
		>
			<div className="scrollbar-inner">
				<CardBox 
				// variant="column"
				>
					<CardBox.Channel title="Первый канал" img={FirstChannel}
						programs={[
							{
								title: 'Новости (с субтитрами)',
								time: '13:00',
								isActive: true
							},
							{
								title: 'Давай поженимся',
								time: '14:00',
							},
							{
								title: 'Другие новости',
								time: '15:00',
							},
						]} />
					<CardBox.Channel title="2x2" img={Channel2x2}
						programs={[
							{
								title: 'МУЛЬТ ТВ. Сезон 4, 7 серия',
								time: '13:00',
								isActive: true
							},
							{
								title: 'ПОДОЗРИТЕЛЬНАЯ СОВА. Сезон 7, 7 серия',
								time: '14:00',
							},
							{
								title: 'БУРДАШЕВ. Сезон 1, 20 серия',
								time: '15:00',
							},
						]} />
					<CardBox.Channel title="РБК" img={RBC}
						programs={[
							{
								title: 'ДЕНЬ. Горючая смесь: как бороться с суррогатом на АЗС',
								time: '13:00',
								isActive: true
							},
							{
								title: 'ДЕНЬ. Главные темы',
								time: '14:00',
							},
							{
								title: 'Главные новости',
								time: '15:00',
							},
						]} />
					<CardBox.Channel title="AMEDIA PREMIUM" img={AmediaPremium}
						programs={[
							{
								title: 'Клиент всегда мёртв',
								time: '13:00',
								isActive: true
							},
							{
								title: 'Голодные игры: Сойка-пересмешница. Часть I',
								time: '14:00',
							},
							{
								title: 'Секс в большом городе',
								time: '15:00',
							},
						]} />
						<CardBox.Channel title="AMEDIA PREMIUM" img={AmediaPremium}
						programs={[
							{
								title: 'Клиент всегда мёртв',
								time: '13:00',
								isActive: true
							},
							{
								title: 'Голодные игры: Сойка-пересмешница. Часть I',
								time: '14:00',
							},
							{
								title: 'Секс в большом городе',
								time: '15:00',
							},
						]} />
						<CardBox.Channel title="AMEDIA PREMIUM" img={AmediaPremium}
						programs={[
							{
								title: 'Клиент всегда мёртв',
								time: '13:00',
								isActive: true
							},
							{
								title: 'Голодные игры: Сойка-пересмешница. Часть I',
								time: '14:00',
							},
							{
								title: 'Секс в большом городе',
								time: '15:00',
							},
						]} />
				</CardBox>
			</div>
		</Scrollbars>
	</div>
};
