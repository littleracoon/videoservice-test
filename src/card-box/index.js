import React from 'react';
import classNames from 'classnames';

import Channel from './cards/channel';
import Film from './cards/film';
import Genre from './cards/genre';

import './main.scss';

function CardBox({ variant, className, children }) {
    return <div className={classNames(
        'card-box',
        { [`card-box--${variant}`]: variant },
        className)} >{children}</div>
}

CardBox.Channel = Channel;
CardBox.Film = Film;
CardBox.Genre = Genre;

export default CardBox;