import React from 'react';
import classNames from 'classnames';

import './genre.scss';

export default function Genre({ title, icon, variant, col, className }) {
    return <div className={classNames('card-box__card', 'genre', { [`card-box__card--col-${col}`]: col }, className)} >
        <a href="#"
            className={classNames('genre__body', { [`genre__body--${variant}`]: variant })}
            onClick={(event) => event.preventDefault()}
        >
            <span role="img" aria-label="fire" className="genre__icon">
                {icon}
            </span>
            <div className="genre__title">{title}</div>
        </a>

    </div>
}