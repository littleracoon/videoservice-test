import React from 'react';
import classNames from 'classnames';

import './film.scss';

export default function Film({ title, img, className, col, children }) {
    return <div className={classNames('card-box__card', 'film', { [`card-box__card--col-${col}`]: col }, className)}>
        <div className="film__body">
            <div className="film__img" style={{ background: 'url(' + img + ')' }}>
            </div>
            <a href="#" className="film__img-overlay" onClick={(event) => event.preventDefault()}>
                <span className="film__description">
                    {children}
                </span>
            </a>
        </div>
        <div className="film__title">{title}</div>
    </div>
}