import React from 'react';
import classNames from 'classnames';

import './channel.scss';

export default function Channel({ title, img, programs = [], className }) {
    return <div className={classNames('card-box__card', 'channel', className)}>
        <div className="channel__logo channel-logo">
            <img className="channel-logo__img" src={img} alt={title} />
        </div>
        <div className="channel__body">
            <div className="channel__title">{title}</div>
            <div className="channel-programs">
                {programs.map((program, index) => (<div
                    key={index}
                    className={classNames(
                        'channel-program',
                        { 'channel-program--active': program.isActive }
                    )} >
                    <div className="channel-program__time"
                    >{program.time}</div>{'   '}<div
                        className="channel-program__text"
                    >{program.title}</div>
                </div>))}
            </div>
        </div>
    </div>
}