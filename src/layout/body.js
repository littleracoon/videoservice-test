import React from 'react';

export default function Body({ children }) {

    return <div className="page__body">{children}</div>
}