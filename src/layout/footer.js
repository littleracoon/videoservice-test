import React from 'react';

export default function Footer({ children }) {

    return <footer className="page__footer">{children}</footer>
}