import React from 'react';
import { NavLink } from "react-router-dom";

import './tabs.scss';

export default function Tabs({ items }) {
    return <div className="page__tabs">
        <div className="container">
            <div className="tabs-content">
                {Object.keys(items).map(
                    item => <NavLink className="tabs-content-item" key={item} to={`/${item}`} activeClassName="tabs-content-item--active">
                        {items[item].name}
                    </NavLink>
                )}
            </div>
        </div>
    </div>
}