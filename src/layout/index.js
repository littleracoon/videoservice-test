import React from 'react';

import Header from './header';
import Tabs from './tabs';
import Body from './body';
import Footer from './footer';

import './main.scss';

function Page({ children }) {
    return <div className="page">{children}</div>
}

Page.Header = Header;
Page.Tabs = Tabs;
Page.Body = Body;
Page.Footer = Footer;

export default Page;

