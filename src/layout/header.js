import React from 'react';

export default function Header({ children }) {

    return <header className="page__header">{children}</header>
}